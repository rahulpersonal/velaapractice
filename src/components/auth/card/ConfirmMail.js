import React from 'react';
import ConfirmMailContent from '../ConfirmMailContent';
import AuthCardLayout from '../../../layouts/AuthCardLayout';
import { Link } from 'react-router-dom';
//import ForgetPasswordForm from '../../auth/ForgetPasswordForm';

const ConfirmMail = () => (

  <AuthCardLayout
    leftSideContent={
      <p className="mb-0 mt-4 mt-md-5 fs--1 font-weight-semi-bold text-300">
        Read our{' '}
        <Link className="text-underline text-300" to="#!">
        Terms of Services
        </Link>{' '}
        and{' '}
        <Link className="text-underline text-300" to="#!">
        Privacy Policy{' '}
        </Link>
      </p>
    }
  >
    <div className="text-center">
      <ConfirmMailContent layout="card" email={''} titleTag="h3" />
    </div>
  </AuthCardLayout>
);

export default ConfirmMail;
