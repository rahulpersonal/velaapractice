import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import rocket from '../../assets/img/illustrations/rocket.png';
import { Col, Row } from 'reactstrap';

const LogoutContent = ({ layout, titleTag: TitleTag }) => {
  return (
    <Fragment>
      <img className="d-block mx-auto mb-4" src={rocket} alt="shield" width={70} />
      <TitleTag>See you again!</TitleTag>
      <p>
      Thanks for using velaa.io. You are now successfully signed out
      </p>
      {/* <Row className="justify-content-between align-items-center"> */}
      <Row>
      <Col xs="auto" sm={{offset: 2}}>
      <Button tag={Link} color="primary" size="sm" className="mt-3" to={`/authentication/${layout}/login`}>
        <FontAwesomeIcon icon="chevron-left" transform="shrink-4 down-1" className="mr-1" />
        Return to Login
      </Button>
      </Col>
      <Col xs="auto" sm={{offset: 0.5}}>
      <Button tag={Link} color="primary" size="sm" className="mt-3" to={`http://reactstrap.github.io/components/layout/`}>
        {/* <FontAwesomeIcon icon="chevron-right" transform="shrink-4 down-2" className="mr-1" /> */}
        Go to velaa.io &nbsp;
        <FontAwesomeIcon icon="chevron-right" transform="shrink-4 down-1" className="mr-1" />
      </Button>
      </Col>
      </Row>
    </Fragment>
  );
};

LogoutContent.propTypes = {
  layout: PropTypes.string,
  titleTag: PropTypes.string
};

LogoutContent.defaultProps = {
  layout: 'basic',
  titleTag: 'h4'
};

export default LogoutContent;
